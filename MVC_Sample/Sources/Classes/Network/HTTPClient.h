//
//  HTTPClient.h
//  MVC_Sample
//
//  Created by Lloyd Presly S on 31/07/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTTPClient : NSObject

- (void)downloadDataAtUrl:(NSString *)url withCompletion:(void (^)(NSError *error, id data))completion;
- (UIImage *)downloadImage:(NSString *)imageUrl;

@end
