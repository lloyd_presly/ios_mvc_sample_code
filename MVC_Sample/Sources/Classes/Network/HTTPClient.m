//
//  HTTPClient.m
//  MVC_Sample
//
//  Created by Lloyd Presly S on 31/07/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import "HTTPClient.h"

@implementation HTTPClient

- (void)downloadDataAtUrl:(NSString *)urlString withCompletion:(void (^)(NSError *error, id data))completion
{
	NSURLSession *session = [NSURLSession sharedSession];
	NSURL *url = [NSURL URLWithString:urlString];
	NSURLSessionDataTask *task = [session dataTaskWithURL:url
										completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
	{
		if (!error)
		{
			NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
			completion(nil, json);
		} else {
			completion(error, nil);
		}
	}];
	
	[task resume];
}

- (UIImage *)downloadImage:(NSString *)imageUrl
{
	NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
	return [UIImage imageWithData:data];
}

@end
