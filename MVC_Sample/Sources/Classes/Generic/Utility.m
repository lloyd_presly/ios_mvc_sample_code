//
//  Utility.m
//  MVC_Sample
//
//  Created by Lloyd Presly S on 14/08/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import "Utility.h"

@implementation Utility

+ (NSString *)homeAPI
{
	return [NSString stringWithFormat:@"%@%@",BaseURLString,HomeAPI];
}

@end
