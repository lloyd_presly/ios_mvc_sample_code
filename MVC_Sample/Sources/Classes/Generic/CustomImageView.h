//
//  CustomImageView.h
//  MVC_Sample
//
//  Created by Lloyd Presly S on 11/08/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomImageView : UIImageView

@property(nonatomic, strong) NSString *imageUrlString;

@end
