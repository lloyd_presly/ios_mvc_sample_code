//
//  Utility.h
//  MVC_Sample
//
//  Created by Lloyd Presly S on 14/08/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject

+ (NSString *)homeAPI;

@end
