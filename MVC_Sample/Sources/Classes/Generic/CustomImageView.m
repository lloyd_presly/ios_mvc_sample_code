//
//  CustomImageView.m
//  MVC_Sample
//
//  Created by Lloyd Presly S on 11/08/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import "CustomImageView.h"
#import "HTTPClient.h"
#import "PersistencyManager.h"

@interface CustomImageView ()
{
	HTTPClient *httpClient;
	PersistencyManager *persistencyManager;
	UIActivityIndicatorView *_activityIndicator;
}
@end

@implementation CustomImageView

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.clipsToBounds = YES;
	if(self.imageUrlString)
	{
		[self downloadImage];
	}
}

- (void)startActivityIndicator
{
	_activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	_activityIndicator.color = [UIColor redColor];
	_activityIndicator.center = self.center;
	[self addSubview:_activityIndicator];
	[self bringSubviewToFront:_activityIndicator];
	[self addObserver:self forKeyPath:@"image" options:0 context:nil];
	[_activityIndicator startAnimating];
}

- (void)setImageUrlString:(NSString *)imageUrlString
{
	_imageUrlString = imageUrlString;
	persistencyManager = [[PersistencyManager alloc] init];
	self.image = [persistencyManager getImage:[_imageUrlString lastPathComponent]];
	
	if (!self.image) {
		if(self.imageUrlString)
		{
			httpClient = [[HTTPClient alloc] init];
			[self downloadImage];
		}
	}
}

- (void)downloadImage
{
	[self startActivityIndicator];
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		UIImage *image = [httpClient downloadImage:_imageUrlString];
		dispatch_sync(dispatch_get_main_queue(), ^{
			self.image = image;
			[persistencyManager saveImage:image filename:[_imageUrlString lastPathComponent]];
		});
	});
}

- (void)dealloc
{
	[self removeObserver:self forKeyPath:@"image"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
					  ofObject:(id)object
						change:(NSDictionary *)change
					   context:(void *)context
{
	
	if ([keyPath isEqualToString:@"image"])
	{
		[_activityIndicator stopAnimating];
	}
}

@end
