//
//  NavigationController.m
//  MVC_Sample
//
//  Created by Lloyd Presly S on 31/07/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import "NavigationController.h"

@interface NavigationController ()

@property (nonatomic, assign) BOOL isMenuOpen;

@end

@implementation NavigationController

static NavigationController *sharedInstance = nil;

+ (NavigationController *)sharedInstance
{
	if(!sharedInstance)
	{
		NSLog(@"Not initialised yet");
	}
	return sharedInstance;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{
		sharedInstance = self;
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	self.delegate = self;
	_isMenuOpen = NO;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
		// Dispose of any resources that can be recreated.
}

- (void)setMenuController:(MenuViewController *)menuController
{
	[menuController.view removeFromSuperview];
	_menuController = menuController;
}

- (void)navigationController:navigationController willShowViewController:(nonnull UIViewController *)viewController animated:(BOOL)animated
{
	viewController.navigationItem.leftBarButtonItem = [self leftBarMenuButton];
	
//	viewController.navigationItem.title = @"Home";
}

- (UIBarButtonItem *)leftBarMenuButton
{
	UIBarButtonItem *customItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"]
																   style:UIBarButtonItemStylePlain
																  target:self
																  action:@selector(leftButtonSelected)];
	return customItem;
}

//- (UIBarButtonItem *)leftBackButton
//{
//	UIBarButtonItem *customItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]
//																   style:UIBarButtonItemStylePlain
//																  target:self
//																  action:@selector(backButtonSelected)];
//	return customItem;
//}

//- (void)backButtonSelected
//{
//	[self closeMenu];
//	[self popViewControllerAnimated:YES];
//}

- (void)leftButtonSelected
{
	if (_isMenuOpen)
	{
		[self closeMenu];
	}
	else
	{
		[self openMenu];
	}
}

- (void)openMenu
{
	CGFloat width = _menuController.menuSize.width;
	[self.view.window insertSubview:self.menuController.view atIndex:0];
	[UIView animateWithDuration:0.25f
					 animations:^{
						 CGRect frame = self.view.frame;
						 frame.origin.x = width;
						 self.view.frame = frame;
					 } completion:^(BOOL finished) {
						 _isMenuOpen = YES;
					 }];
}

- (void)closeMenu
{
	[UIView animateWithDuration:0.25f
					 animations:^{
						 CGRect frame = self.view.frame;
						 frame.origin.x = 0;
						 self.view.frame = frame;
					 } completion:^(BOOL finished) {
						 _isMenuOpen = NO;
					 }];
}

- (void)switchViewController:(BaseController *)viewController
{
	NSArray *viewControllers = self.viewControllers;
	for (BaseController *VC in viewControllers) {
		[VC removeFromParentViewController];
	}
	[self closeMenu];
	[self pushViewController:viewController animated:YES];
}

- (void)pushViewController:(BaseController *)viewController
{
	[self closeMenu];
	[self pushViewController:viewController animated:YES];
	
}
@end
