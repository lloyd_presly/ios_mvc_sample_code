//
//  NavigationController.h
//  MVC_Sample
//
//  Created by Lloyd Presly S on 31/07/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuViewController.h"
#import "BaseController.h"

@interface NavigationController : UINavigationController <UINavigationControllerDelegate>

@property (nonatomic, strong) MenuViewController *menuController;

+ (NavigationController *)sharedInstance;

- (void)switchViewController:(BaseController *)viewController;

- (void)pushViewController:(BaseController *)viewController;


@end
