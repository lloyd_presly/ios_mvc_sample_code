//
//  HomeModel.m
//  MVC_Sample
//
//  Created by Lloyd Presly S on 31/07/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import "HomeModel.h"

@interface HomeModel ()

@end

@implementation HomeModel

- (void)setModelWithDictionary:(NSDictionary *)modelDictionary
{
	[super setModelWithDictionary:modelDictionary];
	if (modelDictionary &&  modelDictionary[@"categoryImage"])
	{
		_coverUrl = modelDictionary[@"categoryImage"];
	}
}

@end
