//
//  HomeInteractor.h
//  MVC_Sample
//
//  Created by Lloyd Presly S on 31/07/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HomeViewController.h"

@interface HomeInteractor : NSObject

@property (nonatomic, weak) HomeViewController *homeViewController;

+ (HomeInteractor *)sharedInstance;

- (void)downloadData;

@end
