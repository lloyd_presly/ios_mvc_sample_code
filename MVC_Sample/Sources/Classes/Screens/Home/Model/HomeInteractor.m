//
//  HomeInteractor.m
//  MVC_Sample
//
//  Created by Lloyd Presly S on 31/07/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import "HomeInteractor.h"

#import "HTTPClient.h"
#import "PersistencyManager.h"

#import "HomeModel.h"

@interface HomeInteractor ()
{
	HTTPClient *httpClient;
	PersistencyManager *persistencyManager;
	NSString *_urlString;
	NSDictionary *_homeData;
	NSError *_err;
}

@end

@implementation HomeInteractor

+ (HomeInteractor *)sharedInstance
{
	static HomeInteractor *_sharedInstance = nil;
	
	static dispatch_once_t once;
	
	dispatch_once(&once, ^{
		_sharedInstance = [[HomeInteractor alloc] init];
	});
	
	return _sharedInstance;
}

- (id)init
{
	self = [super init];
	
	if (self)
	{
		persistencyManager = [[PersistencyManager alloc] init];
		httpClient = [[HTTPClient alloc] init];
	    _urlString = [Utility homeAPI];
		_homeData = nil;
		_err = nil;
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(downloadDataSuccess:)
													 name:@"DownloadDataSuccessNotification"
												   object:nil];
	}
	
	return self;
}

- (void)downloadData
{
	[httpClient downloadDataAtUrl:_urlString withCompletion:^(NSError *error, id data) {
		_err = error;
		_homeData = data;
		[[NSNotificationCenter defaultCenter] postNotificationName:@"DownloadDataSuccessNotification"
															object:self];
	}];
}

- (void)downloadDataSuccess:(NSNotification *)notification
{
	NSMutableArray *dataArray = [[NSMutableArray alloc] init];
	NSArray *data = _homeData[@"data"];
	for (NSDictionary *dictionary in data)
	{
		HomeModel *model = [[HomeModel alloc] init];
		[model setModelWithDictionary:dictionary];
		[dataArray addObject:model];
	}
	_homeViewController.title = @"Fruits";
	[_homeViewController updateDownloadData:dataArray];
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
