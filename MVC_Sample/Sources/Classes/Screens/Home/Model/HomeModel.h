//
//  HomeModel.h
//  MVC_Sample
//
//  Created by Lloyd Presly S on 31/07/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import "BaseModel.h"

@interface HomeModel : BaseModel

@property (nonatomic, strong) NSString *coverUrl;

@end
