//
//  HomeCell.h
//  MVC_Sample
//
//  Created by Lloyd Presly S on 02/08/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import "BaseCell.h"

@interface HomeCell : BaseCell

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

- (void)updateUI;

@end
