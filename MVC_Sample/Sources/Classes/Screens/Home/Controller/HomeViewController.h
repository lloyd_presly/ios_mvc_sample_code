//
//  HomeViewController.h
//  MVC_Sample
//
//  Created by Lloyd Presly S on 31/07/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import "BaseController.h"

@interface HomeViewController : BaseController

@property (nonatomic, weak) IBOutlet UICollectionView *colletionView;

@end
