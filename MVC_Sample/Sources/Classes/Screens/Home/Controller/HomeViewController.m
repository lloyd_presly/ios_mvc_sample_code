//
//  HomeViewController.m
//  MVC_Sample
//
//  Created by Lloyd Presly S on 31/07/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeInteractor.h"
#import "HomeModel.h"
#import "HomeCell.h"

@interface HomeViewController ()<UICollectionViewDataSource>
{
	NSArray *_data;
	HomeInteractor *homeInteractorSharedInstance;
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	homeInteractorSharedInstance = [HomeInteractor sharedInstance];
	[homeInteractorSharedInstance setHomeViewController:self];
	[homeInteractorSharedInstance downloadData];
}

- (void)updateDownloadData:(NSArray *)downloadedData
{
	_data = downloadedData;
	if (_data) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[_colletionView reloadData];
		});
	}
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return _data.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	HomeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeCell" forIndexPath:indexPath];
	cell.titleLabel.text = ((HomeModel *)_data[indexPath.row]).name;
	cell.tag = indexPath.row;
	[cell initWithImageUrl:((HomeModel *)_data[indexPath.row]).coverUrl];
	return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
