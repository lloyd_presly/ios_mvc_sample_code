//
//  MenuViewController.m
//  MVC_Sample
//
//  Created by Lloyd Presly S on 31/07/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuCell.h"
#import "BaseController.h"
#import "NavigationController.h"

@interface MenuViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray *menuItems;

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	_menuItems = @[@"Home",@"Other"];
	[_tableView reloadData];
}

- (CGSize)menuSize
{
	return _tableView.frame.size;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return _menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:Cell forIndexPath:indexPath];
	[cell setMenuItem:self.menuItems[indexPath.row]];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	/*UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
	BaseController *VC = nil;
	switch (indexPath.row) {
		case 0:
		{
			VC = [storyBoard instantiateViewControllerWithIdentifier:@"ViewController"];
			VC.titleText = @"HOME";
			break;
		}
		case 1:
		{
			VC = [storyBoard instantiateViewControllerWithIdentifier:@"SecondViewController"];
			VC.titleText = @"OTHER";
			break;
		}
		default:
		{
			break;
		}
	}
	[[NavigationController sharedInstance] switchViewController:VC];*/
}

@end
