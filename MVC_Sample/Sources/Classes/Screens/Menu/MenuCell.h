//
//  MenuCell.h
//  MVC_Sample
//
//  Created by Lloyd Presly S on 31/07/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property (nonatomic, strong) NSString *menuItem;

@property (nonatomic, weak) IBOutlet UILabel *label;

- (void)setMenuItem:(NSString *)menuItem;

@end
