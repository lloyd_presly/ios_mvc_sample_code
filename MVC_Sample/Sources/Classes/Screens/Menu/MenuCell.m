//
//  MenuCell.m
//  MVC_Sample
//
//  Created by Lloyd Presly S on 31/07/16.
//  Copyright © 2016 DingChak. All rights reserved.
//


#import "MenuCell.h"

@implementation MenuCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setMenuItem:(NSString *)menuItem
{
	_menuItem = menuItem;
	[self updateUI];
}

- (void)updateUI
{
	_label.text = _menuItem;
}

@end
