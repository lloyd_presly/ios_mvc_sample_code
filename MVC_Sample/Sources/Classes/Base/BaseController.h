//
//  BaseController.h
//  MVC_Sample
//
//  Created by Lloyd Presly S on 31/07/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseController : UIViewController

@property (nonatomic, strong) NSString *titleText;

- (void)updateDownloadData:(NSArray *)downloadedData;

@end
