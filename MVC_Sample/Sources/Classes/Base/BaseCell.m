//
//  BaseCell.m
//  MVC_Sample
//
//  Created by Lloyd Presly S on 02/08/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import "BaseCell.h"

@interface BaseCell ()
{
}

@end

@implementation BaseCell

- (void)initWithImageUrl:(NSString *)imageUrl
{
	self.imageView.backgroundColor = [UIColor whiteColor];
	if (imageUrl)
	{
		self.imageView.imageUrlString = imageUrl;
	}
	else
	{
		NSLog(@"image:%@",imageUrl);
		_imageView.image = nil;
		self.imageView.backgroundColor = [UIColor redColor];
	}
}

@end
