//
//  BaseModel.m
//  MVC_Sample
//
//  Created by Lloyd Presly S on 31/07/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import "BaseModel.h"

@interface BaseModel ()

@end

@implementation BaseModel

- (void)setModelWithDictionary:(NSDictionary *)modelDictionary
{
	if (modelDictionary &&  modelDictionary[@"_id"])
	{
		_id = modelDictionary[@"_id"];
	}
	if (modelDictionary &&  modelDictionary[@"name"])
	{
		_name = modelDictionary[@"name"];
	}
}

@end
