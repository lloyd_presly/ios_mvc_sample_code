//
//  BaseCell.h
//  MVC_Sample
//
//  Created by Lloyd Presly S on 02/08/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomImageView.h"

@interface BaseCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet CustomImageView *imageView;

- (void)initWithImageUrl:(NSString *)imageUrl;

@end
