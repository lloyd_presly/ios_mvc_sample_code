//
//  PersistencyManager.m
//  MVC_Sample
//
//  Created by Lloyd Presly S on 03/08/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import "PersistencyManager.h"

@implementation PersistencyManager

- (id)init
{
	self = [super init];
	if (self)
	{
	}
	return self;
}

- (void)saveImage:(UIImage*)image filename:(NSString*)filename
{
	filename = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@", filename];
	NSData *data = UIImagePNGRepresentation(image);
	[data writeToFile:filename atomically:YES];
}

- (UIImage*)getImage:(NSString*)filename
{
	filename = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@", filename];
	NSData *data = [NSData dataWithContentsOfFile:filename];
	return [UIImage imageWithData:data];
}

@end
