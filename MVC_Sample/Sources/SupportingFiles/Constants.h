//
//  Constants.h
//  MVC_Sample
//
//  Created by Lloyd Presly S on 14/08/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const BaseURLString;
FOUNDATION_EXPORT NSString *const HomeAPI;
FOUNDATION_EXPORT NSString *const Cell;
