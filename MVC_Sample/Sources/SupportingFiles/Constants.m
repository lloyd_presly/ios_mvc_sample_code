//
//  Constants.m
//  MVC_Sample
//
//  Created by Lloyd Presly S on 14/08/16.
//  Copyright © 2016 DingChak. All rights reserved.
//

#import "Constants.h"

NSString *const BaseURLString = @"http://ec2-52-43-79-87.us-west-2.compute.amazonaws.com:8080/api/v1";
NSString *const HomeAPI = @"/categories?skip=30&limit=50";
NSString *const Cell = @"cell";
